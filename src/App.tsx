import { WagmiConfig, createConfig } from "wagmi";
import { bsc } from "wagmi/chains";
import { createPublicClient, http } from "viem";
import Home from "@/Home";

const config = createConfig({
  autoConnect: true,
  publicClient: createPublicClient({
    chain: bsc,
    transport: http(),
  }),
});

function App() {
  return (
    <WagmiConfig config={config}>
      <Home></Home>
    </WagmiConfig>
  );
}

export default App;
