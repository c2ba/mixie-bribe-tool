import { useAccount, useConnect, useDisconnect } from "wagmi";
import { InjectedConnector } from "wagmi/connectors/injected";
import { usePublicClient } from "wagmi";
import { getAddress, formatEther } from "viem";
import { useContractReads } from "wagmi";
import { AbiGaugeV2 } from "@/abi/GaugeV2";
import { useMemo } from "react";

const addresses = {
  c2ba: getAddress("0x49a5492FDFe5AcC966dD5f41310dfDfe8dAA349C"),
  pnf: getAddress("0x8Fcb3F6b7E6c6bce5c0CE935a653Cac2C3e7ee07"),
  mixGauges: {
    eth: getAddress("0xb3c144c0ae30ed51b4250816202b847b97b829da"),
    matic: getAddress("0xe072405ed4f83db9f86519de164ecd6cbaef42e6"),
    the: getAddress("0x59057c2a13e1682a2f2b30d12d9852840d613aa2"),
    usdc: getAddress("0x209d478bbdfee5a881f7ea570439b387d6623566"),
    bnb: getAddress("0x4be31cd6ccc28eec41dd9cb8a74d788c83b01db0"),
  },
};

// Lire les exemple de viem voir si il y a mieux pour read, sinon implementer mon truc

// Metriques que je veux:
// - TVL de chaque pool, TVL de chaque entitée (c2ba, pnf, others)
// - Tenter de recup avec logs les THE de chaque pool et afficher l'APR, et les THE alloué a chaque entitée
// - Cout en TVL pour monter de 1% d'ownership, ou pour atteindre un % voulu
// - Affichage des stats passées (stockée en dur, puis tentons de fetch) de Thena, en particulier la rentabilité des bribes
// - Calcul du montant de bribes à injecter pour atteindre un APR donné, calcul du share requis

export default function Home() {
  const { data, error, isError, isLoading } = useContractReads({
    contracts: Object.values(addresses.mixGauges)
      .map((address) => ({
        address,
        abi: AbiGaugeV2,
        functionName: "totalSupply",
      }))
      .concat(
        Object.values(addresses.mixGauges).map((address) => ({
          address,
          abi: AbiGaugeV2,
          functionName: "balanceOf",
          args: [addresses.c2ba],
        }))
      )
      .concat(
        Object.values(addresses.mixGauges).map((address) => ({
          address,
          abi: AbiGaugeV2,
          functionName: "balanceOf",
          args: [addresses.pnf],
        }))
      ),
  });

  const processedData = useMemo(() => {
    if (data === undefined) {
      return undefined;
    }
    const dataAsNumbers = data.map((item) => parseFloat(formatEther(item.result as bigint)));
    const gaugeCount = Object.keys(addresses.mixGauges).length;
    return {
      shares: {
        c2ba: Object.fromEntries(dataAsNumbers.slice(gaugeCount, 2 * gaugeCount).map((balance, idx) => [Object.keys(addresses.mixGauges)[idx], balance / dataAsNumbers[idx]])),
        pnf: Object.fromEntries(dataAsNumbers.slice(2 * gaugeCount, 3 * gaugeCount).map((balance, idx) => [Object.keys(addresses.mixGauges)[idx], balance / dataAsNumbers[idx]])),
      },
    };
  }, [data]);

  return isLoading ? (
    <div>Loading...</div>
  ) : isError || processedData === undefined ? (
    <div>{error?.message}</div>
  ) : (
    <div className="w-full flex flex-col place-items-center gap-2">
      <h1>LP Share</h1>
      <table className="w-full table-fixed text-center">
        <thead>
          <tr>
            <th></th>
            <th>c2ba</th>
            <th>pnf</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(addresses.mixGauges).map((gaugeName) => (
            <tr>
              <th>{gaugeName}</th>
              <td>{(100 * processedData.shares.c2ba[gaugeName]).toFixed(2)} %</td>
              <td>{(100 * processedData.shares.pnf[gaugeName]).toFixed(2)} %</td>
              <td>{(100 * (processedData.shares.c2ba[gaugeName] + processedData.shares.pnf[gaugeName])).toFixed(2)} %</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
